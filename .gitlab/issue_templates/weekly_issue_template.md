**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ]
- [ ]
- [ ]

**Every week tasks:**
***Monday:***
- [ ] Alex 1:1 prep
- [ ] linear meeting prep
- [ ] Check recruitment

***Tuesday:***
- [ ] Linear numbers
- [ ] Engineering allocation meeting prep
- [ ] Check error budget

***Wednesday:***
- [ ] Serena 1:1 prep
- [ ] Workspace project weekly prep

***Thursday:***
- [ ] triaging
- [ ] Michelle 1:1 prep

***Friday:***
- [ ] OKRs update
- [ ] Orit 1:1 prep

**If time permits:**
- [ ]

**This week learnings:**
-
